# Overview

| Component | Package | Config Directory |
| --- | --- | --- |
| Window Manager | [Xmonad](https://github.com/xmonad/xmonad "Xmonad Github") | [~/.xmonad](https://gitlab.com/Zukkii/dots/-/tree/main/home/.xmonad "Xmonad config") |
| Bar | [Polybar](https://github.com/polybar/polybar "Polybar Github") | [~/.config/polybar](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/polybar "Polybar config") |
| Menu | [Rofi](https://github.com/davatorium/rofi "Rofi Github") | [~/.config/rofi](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/rofi "Rofi config") |
| Compositor | [Picom](https://github.com/jonaburg/picom "Picom Animation Github") | [~/.config/picom](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/picom "Picom config") |
| Text Editor / IDE | [Neovim](https://github.com/neovim/neovim "Neovim Github") | [~/.config/nvim](https://gitlab.com/Zukkii/dots/-/tree/main/user/.config/nvim "Neovim config") |
| Terminal | [Kitty](https://github.com/kovidgoyal/kitty "Kitty Github") | [~/.config/kitty](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/kitty "Kitty config") |
| Shell | [Zsh](https://github.com/zsh-users/zsh "Zsh Github") | [~/.config/zsh](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/zsh "Zsh Config") |
| File Manager | [Vifm](https://github.com/vifm/vifm "Vifm Github") | [~/.config/vifm](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/vifm "Vifm config") |
| Notification | [Dunst](https://github.com/dunst-project/dunst "Dunst Github") | [~/.config/dunst](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/dunst "Dunst config") |
| PDF Reader | [Zathura](https://github.com/pwmt/zathura "Zathura Github") | [~/.config/zathura](https://gitlab.com/Zukkii/dots/-/tree/main/system/.config/zathura "Zathura config") |

* * *

# GUI Applications

- **Browser -** Microsoft Edge Chromium, Librewolf
- **File Manager -** Nemo
- **Notes -** Joplin
- **Communication -** Caprine, Discord
- **Office -** LibreOffice, WPS Office
- **Calculator -** Qalculate!
- **Media Player -** MPV
- **Video Editor -** Kdenlive
- **Audio Editor -** Audacium
- **Image Manipulation -** GIMP
- **Art (Vector / Painting) -** Krita, Inkscape
- **Screen Recording -** OBS Studio
- **Display Manager -** Lightdm
- **Greeter -** Lightdm GTK Greeter

* * *

# Setup and Installation

### Pre-requisites

- Git
- Stow

### Arch

`sudo pacman -S git stow`

### Debian | Ubuntu

`sudo apt-get update`

`sudo apt-get install git stow`

* * *

### Clone Dotfiles

`git clone https://gitlab.com/Zukkii/dots.git && cd dots`

### Install Configs

`stow -v home system user`

### Uninstall

`stow -vD home system user`

* * *
