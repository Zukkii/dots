-- general
vim.opt.updatetime = 300      -- faster completion
vim.opt.smartcase = true      -- smart case
vim.opt.smartindent = true    -- make indenting smarter again
vim.opt.relativenumber = true -- set relative numbered lines
vim.opt.wrap = true           -- display lines as one long line
-- lvim.lsp.diagnostics.virtual_text = false;
vim.diagnostic.config({ virtual_text = false })

lvim.log.level = "warn"
lvim.format_on_save.enabled = true
lvim.colorscheme = "ayu-mirage"
lvim.transparent_window = true
-- lvim.use_icons = false
--
-- Terminal settings
lvim.builtin.terminal.direction = "horizontal"
lvim.builtin.terminal.size = 10
lvim.builtin.terminal.active = true

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"

-- nvim-tree
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true
lvim.builtin.nvimtree.setup.renderer.add_trailing = true
lvim.builtin.nvimtree.setup.renderer.indent_markers.enable = true
lvim.builtin.nvimtree.setup.renderer.indent_markers.icons = {
  corner = "└ ",
  edge = "│ ",
  item = "│ ",
  none = "  ",
}

-- generic LSP settings

-- make sure server will always be installed even if the server is in skipped_servers list
lvim.lsp.installer.setup.ensure_installed = {
  "lua_ls",
}

-- ---@usage disable automatic installation of servers
-- lvim.lsp.installer.setup.automatic_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright",

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- Additional Plugins
-- vim.lsp.handlers["textDocument/publishDiagnostics"] = function()
-- end

reload("user.null-ls")
reload("user.keybinds")
reload("user.cmp")
reload("user.rainbow")
reload("user.colorizer")
reload("user.treesitter")
reload("user.indentlines")
reload("user.omnisharp")
reload("user.autocmd")
reload("user.coderun")
reload("user.plugins")
reload("user.lspsaga")
-- reload("user.uiborders")
