-- vim.cmd([[ let @r="\y:%s/\<C-r>\"//g\<Left>\<Left>" ]]) -- replace all occurrences (replaced with treesitter)
--vim.cmd [[ command GoBuildTest execute "ProjectRoot \<CR> :w\<CR> :sp | terminal  go run $(ls -1 *.go | grep -v _test.go) \<CR>i" ]] -- run a go project without building
-- vim.cmd([[ let @1=":call CppComp() \<CR>G:66\<CR>" ]]) -- C++ competitive programming template
vim.cmd([[ let @c=":cd %:h \<CR>" ]]) -- change dir to current file
-- vim.cmd([[ let @p=":ProjectRoot \<CR>:lua require('notify')('Project root setted.',0,{title='EXPLORER',0,timeout=300})\<cr>" ]]) -- cd to root project
vim.cmd([[ let @m=":call MakeRun()\<CR>" ]]) -- make and run
-- vim.cmd([[ let @t=":ProjectRoot \<CR>:! go test ./... -coverprofile=coverage.out && go tool cover -html=coverage.out\<cr>" ]])

-- All previous macros have been changed to autocmd, @g macro will run current file
vim.cmd([[
  augroup bufferwrite
    au!
    autocmd bufwritepost files,directories !shortcuts
    autocmd bufwritepost *xresources !xrdb %
    autocmd bufwritepost dunstrc !pkill dunst; dunst &
    autocmd bufwritepost *sxhkdrc !pkill -usr1 sxhkd
    autocmd bufwritepost *xmonad.hs !xmonad --recompile
  augroup end
]])
