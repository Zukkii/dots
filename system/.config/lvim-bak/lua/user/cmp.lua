-- lvim.builtin.cmp.window = {}

lvim.builtin.cmp.formatting = {
  fields = { "kind", "abbr", "menu" },
  format = function(entry, vim_item)
    -- vim_item.kind = string.format('%s %s', lvim.builtin.cmp.formatting.kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
    vim_item.kind = string.format("%s", lvim.builtin.cmp.formatting.kind_icons[vim_item.kind])
    vim_item.menu = ({
      nvim_lsp = "[LSP]",
      luasnip = "[Snip]",
      buffer = "[Buff]",
      path = "[Path]",
    })[entry.source.name]
    -- local kind = require("lspkind").cmp_format({ mode = "symbol_text", maxwidth = 50 })(entry, vim_item)
    -- local strings = vim.split(kind.kind, "%s", { trimempty = true })
    -- kind.kind = " " .. (strings[1] or "") .. " "
    -- kind.menu = "    (" .. (strings[2] or "") .. ")"
    return vim_item
  end,
}

-- CMP LSP borders
lvim.builtin.cmp.window.completion.border = "single"
lvim.builtin.cmp.window.documentation.border = "single"

-- lvim.builtin.cmp.formatting.kind_icons = {
--   Text = "",
--   Method = "",
--   Function = "",
--   Constructor = "",
--   Field = "",
--   Variable = "",
--   Class = "ﴯ",
--   Interface = "",
--   Module = "",
--   Property = "ﰠ",
--   Unit = "塞",
--   Value = "",
--   Enum = "",
--   Keyword = "",
--   Snippet = "",
--   Color = "",
--   File = "",
--   Reference = "",
--   Folder = "",
--   EnumMember = "",
--   Constant = "",
--   Struct = "פּ",
--   Event = "",
--   Operator = "",
--   TypeParameter = "",
-- }

lvim.builtin.cmp.formatting.kind_icons = {
  Text = "󰉿",
  Method = "󰆧",
  Function = "󰊕",
  Constructor = "",
  Field = "󰜢",
  Variable = "󰀫",
  Class = "󰠱",
  Interface = "",
  Module = "",
  Property = "󰜢",
  Unit = "󰑭",
  Value = "󰎠",
  Enum = "",
  Keyword = "󰌋",
  Snippet = "",
  Color = "󰏘",
  File = "󰈙",
  Reference = "󰈇",
  Folder = "󰉋",
  EnumMember = "",
  Constant = "󰏿",
  Struct = "󰙅",
  Event = "",
  Operator = "󰆕",
  TypeParameter = "",
}
