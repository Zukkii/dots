-- keymappings [view all the defaults by pressing <leader>Lk]

lvim.leader = "space"

lvim.keys.normal_mode["<C-s>"] = ":w<cr>"

-- Custom keybinds

-- Lspsaga bindings
lvim.keys.normal_mode["<C-j>"] = "<Cmd>Lspsaga diagnostic_jump_prev<CR>"
lvim.lsp.buffer_mappings.normal_mode["K"] = nil
lvim.keys.normal_mode["K"] = "<Cmd>Lspsaga hover_doc<CR>"
lvim.keys.normal_mode["Gr"] = "<Cmd>Lspsaga rename<CR>"
lvim.keys.normal_mode["C-k"] = "<Cmd>Lspsaga lsp_finder<CR>"

-- Increment Decrement
lvim.keys.normal_mode["+"] = "<C-a>"
lvim.keys.normal_mode["-"] = "<C-x>"

-- Delete a word backwards
lvim.keys.normal_mode["dw"] = 'vb"_d'

-- Select All
lvim.keys.normal_mode["<C-a>"] = "gg<S-v>G"

-- Run Code
-- lvim.keys.normal_mode["<C-a>"] = "gg<S-v>G"

-- Bufferline keymaps
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"
lvim.keys.normal_mode["<S-A-1>"] = ":BufferLineGoToBuffer 1<CR>"
lvim.keys.normal_mode["<S-A-2>"] = ":BufferLineGoToBuffer 2<CR>"
lvim.keys.normal_mode["<S-A-3>"] = ":BufferLineGoToBuffer 3<CR>"
lvim.keys.normal_mode["<S-A-4>"] = ":BufferLineGoToBuffer 4<CR>"
lvim.keys.normal_mode["<S-A-5>"] = ":BufferLineGoToBuffer 5<CR>"
lvim.keys.normal_mode["<S-A-6>"] = ":BufferLineGoToBuffer 6<CR>"
lvim.keys.normal_mode["<S-A-7>"] = ":BufferLineGoToBuffer 7<CR>"
lvim.keys.normal_mode["<S-A-8>"] = ":BufferLineGoToBuffer 8<CR>"
lvim.keys.normal_mode["<S-A-9>"] = ":BufferLineGoToBuffer 9<CR>"
lvim.keys.normal_mode["<S-A-q>"] = ":bdelete<CR>"

-- lvim.keys.normal_mode[]

-- Open Terminal
lvim.builtin.terminal.open_mapping = "<c-t>"

-- toggle Diagnostic Virtual Text
lvim.builtin.which_key.mappings.l.t = {
	function()
		local config = vim.diagnostic.config()
		if config then
			config.virtual_text = not config.virtual_text
			vim.diagnostic.config(config)
		end
	end,
	"Toggle virtual text",
}
