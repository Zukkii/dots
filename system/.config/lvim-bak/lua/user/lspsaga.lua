require("lspsaga").setup({
	ui = {
		-- This option only works in Neovim 0.9
		title = false,
		-- Border type can be single, double, rounded, solid, shadow.
		border = "single",
		winblend = 0,
		expand = "",
		collapse = "",
		code_action = "💡",
		incoming = " ",
		outgoing = " ",
		hover = " ",
		kind = {},
	},
	hover = {
		max_width = 0.6,
		max_height = 0.2,
		open_link = "gx",
		open_browser = "!librewolf",
	},
})
