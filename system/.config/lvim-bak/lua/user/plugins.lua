lvim.plugins = {

	-- nvim-trouble
	{
		"folke/trouble.nvim",
		cmd = "TroubleToggle",
	},

	-- nvim-spectre
	{
		"windwp/nvim-spectre",
		event = "BufRead",
		config = function()
			require("spectre").setup()
		end,
		-- vim-matchup
		{
			"andymass/vim-matchup",
			event = "CursorMoved",
			config = function()
				vim.g.matchup_matchparen_offscreen = { method = "popup" }
			end,
		},
		-- nvim-ts-rainbow
		{
			"mrjones2014/nvim-ts-rainbow",
		},
		-- lsp-colors
		{
			"folke/lsp-colors.nvim",
			event = "BufRead",
		},
		-- nvim-autotag
		{
			"windwp/nvim-ts-autotag",
			config = function()
				require("nvim-ts-autotag").setup({
					filetypes = { "html", "xml", "javascript", "javascriptreact", "typescript", "typescriptreact" },
				})
			end,
		},

		-- code-runner
		{
			"CRAG666/code_runner.nvim",
			config = true,
		},

		-- lspsaga
		{
			"glepnir/lspsaga.nvim",
			event = "LspAttach",
		},

		-- nvim-colorizer
		{
			"norcalli/nvim-colorizer.lua",
		},
		-- lush
		{
			"rktjmp/lush.nvim",
		},
		-- lsplind
		{
			"onsails/lspkind.nvim",
			config = function()
				require("lspkind")
			end,
		},

    {
      "Shatur/neovim-ayu"
    }
	},
}
