require("nvim-treesitter.configs").setup({
	rainbow = {
		enable = true,
		extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
		colors = {
			"#ecbe7b",
			-- "#da8548",
			-- "#c678dd",
			-- "#a9a1e1",
			-- "#51afef",
			"#46d9ff",
		}, -- table of hex strings
	},
})
