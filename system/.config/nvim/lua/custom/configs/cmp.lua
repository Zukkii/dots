require("cmp").setup({
  window = {
    completion = {
      border = {
        style = "single",
      },
    },
    documentation = {
      border = {
        style = "single",
      },
    },
  }
})
