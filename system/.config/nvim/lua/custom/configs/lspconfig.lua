local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities
local config = require "plugins.configs.lspconfig"

local lspconfig = require "lspconfig"

-- if you just want default config for the servers then put them in a table
local servers = { "html", "cssls", "tsserver", "clangd", "pyright", "eslint" }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,

    init_options = {
      preferences = {
        disableSuggestions = true,
      },
    },
  }
end

--
-- lspconfig.pyright.setup { blabla}

-- lspconfig.tsserver.setup {
--   on_attach = on_attach,
--   capabilities = capabilities
-- }
