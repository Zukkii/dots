local Noice = require "noice"

Noice.setup {
  lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
  },
  -- you can enable a preset for easier configuration
  presets = {
    bottom_search = true, -- use a classic bottom cmdline for search
    command_palette = false, -- position the cmdline and popupmenu together
    long_message_to_split = true, -- long messages will be sent to a split
    inc_rename = true, -- enables an input dialog for inc-rename.nvim
    lsp_doc_border = true, -- add a border to hover docs and signature help
  },

  cmdline = {
    enabled = true,
    view = "cmdline_popup",
  },

  hover = {
    enabled = true,
    silent = false, -- set to true to not show a message if hover is not available
    view = nil, -- when nil, use defaults from documentation
    opts = {
      border = {
        style = "single",
      },
    }, -- merged with defaults from documentation
  },
}
