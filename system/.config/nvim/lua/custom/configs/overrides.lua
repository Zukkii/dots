local M = {}

M.treesitter = {
  ensure_installed = {
    "vim",
    "python",
    "lua",
    "html",
    "css",
    "javascript",
    "typescript",
    "tsx",
    "markdown",
    "markdown_inline",
  },
  indent = {
    enable = true,
    -- disable = {
    --   "python"
    -- },
  },
}

M.mason = {
  ensure_installed = {
    -- lua stuff
    "lua-language-server",
    "stylua",

    -- web dev stuff
    "css-lsp",
    "html-lsp",
    "typescript-language-server",
    "eslint",
    "prettierd",

    -- python
    "pyright",
  },
}

-- git support in nvimtree
M.nvimtree = {
  git = {
    enable = true,
  },

  renderer = {
    highlight_git = true,
    icons = {
      show = {
        git = true,
      },
    },
  },
}

M.cmp = {
  window = {
    completion = {
      border = "single",
    },
    documentation = {
      border = "single",
    },
  },
}

-- M.ui = {
--   changed_themes = {
--     catppuccin = {
--       base_16 = {
--         base05 = "blue",
--         base08 = "blue",
--       },
--       base_30 = {
--         red = "#8bc2f0",
--       },
--     },
--   },
--   hl_override = {
--     Character = {
--       bg = { "blue" },
--     },
--     Variable = {
--       bg = { "purple" },
--     },
--     ["@character"] = {
--       fg = { "blue" },
--     },
--     ["@variable"] = {
--       fg = { "purple" },
--     },
--   },
-- }

return M
