-- local autocmd = vim.api.nvim_create_autocmd

-- Auto resize panes when resizing nvim window
-- autocmd("VimResized", {
--   pattern = "*",
--   command = "tabdo wincmd =",
-- })

-- vim.opt.colorcolumn = "80"
vim.opt.updatetime = 300      -- faster completion
vim.opt.smartcase = true      -- smart case
vim.opt.smartindent = true    -- make indenting smarter again
vim.opt.relativenumber = true -- set relative numbered lines
vim.opt.wrap = true           -- display lines as one long line
vim.diagnostic.config({
  virtual_text = false,
  severity_sort = true,
  float = {
    border = 'single',
    source = 'always',
  },
})
-- vim.diagnostic.config({ virtual_text = true })
