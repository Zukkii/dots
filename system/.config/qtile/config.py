import os
import re
import shutil
import socket
import subprocess
from typing import List  # noqa: F401

from libqtile import bar, hook, layout, qtile, widget
from libqtile.config import EzClick as Click
from libqtile.config import EzDrag as Drag
from libqtile.config import EzKey as Key
from libqtile.config import Group, Match, Rule, Screen
from libqtile.lazy import lazy
from libqtile.widget import Spacer

home = os.path.expanduser("~")
terminal = "kitty"


class Gaps(object):
    # ...
    gap = 1


class WorkIcon(object):
    icon = ""
    icon_active = ""


@lazy.function
def kill_all_windows(qtile):
    for window in qtile.currentGroup.windows:
        window.kill()


keys = [
    # Switch between windows
    # Key("M-h", lazy.layout.left(), desc="Move focus to left"),
    # Key("M-l", lazy.layout.right(), desc="Move focus to right"),
    Key("M-j", lazy.layout.down(), desc="Move focus down"),
    Key("M-k", lazy.layout.up(), desc="Move focus up"),
    Key("M-<space>", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key("M-S-h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key("M-S-l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key("M-S-j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key("M-S-k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key("M-w", lazy.spawn("wal"), desc="Change Wallpaper"),
    Key(
        "M-h",
        lazy.layout.shrink_main(),
        lazy.layout.decrease_ratio(),
        desc="Grow window to the left",
    ),
    Key(
        "M-l",
        lazy.layout.grow_main(),
        lazy.layout.increase_ratio(),
        desc="Grow window to the right",
    ),
    Key("M-m", lazy.layout.maximize(), desc="Grow window to the right"),
    Key("M-C-j", lazy.layout.grow_down(), desc="Grow window down"),
    Key("M-n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key("M-C-k", lazy.layout.grow_up(), desc="Toggle Window to Fullscreen"),
    Key("M-<Space>", lazy.window.toggle_fullscreen(), desc="Grow window up"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key("M-S-q", lazy.spawn("bash ~/.config/rofi/powermenu.sh"),
    # desc="Launch Rofi Terminate Menu"),
    Key(
        "M-S-<Return>",
        lazy.spawn("rofi -show drun -theme ~/.config/rofi/applets/appmenu.rasi"),
        desc="Launch Rofi App Menu",
    ),
    Key("M-<Return>", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key("M-<Tab>", lazy.next_layout(), desc="Toggle between layouts"),
    Key("M-S-c", lazy.window.kill(), desc="Kill focused window"),
    Key("M-S-a", lazy.spawn(kill_all_windows), desc="Kill all windows"),
    Key("M-S-r", lazy.restart(), desc="Restart Qtile"),
    Key("M-S-q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key("M-r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    # Keybindings to launch user defined programs
    Key("A-d", lazy.spawn("dmenu_run"), desc="Launch dmenu"),
    Key("A-e", lazy.spawn("emacs"), desc="Launch emacs"),
    Key("A-f", lazy.spawn("thunar"), desc="Launch thunar"),
    Key(
        "A-m",
        lazy.spawn("/usr/local/src/thunderbird/thunderbird"),
        desc="Launch thunderbird",
    ),
    Key("A-n", lazy.spawn("nitrogen"), desc="Launch nitrogen"),
    Key("A-r", lazy.spawn("rofi -show run"), desc="Launch rofi"),
    Key("A-s", lazy.spawn("st"), desc="Launch suckless terminal"),
    Key("A-t", lazy.spawn("urxvtc"), desc="Launch rxvt-unicode"),
    Key(
        "A-w",
        lazy.spawn("/usr/local/src/waterfox/waterfox-bin"),
        desc="Launch waterfox",
    ),
    Key("A-C-w", lazy.spawn("brave-browser"), desc="Launch brave-browser"),
    Key(
        "A-C-s",
        lazy.spawn("/usr/local/src/sublime_text/sublime_text"),
        desc="Launch sublime_text",
    ),
    Key(
        "<XF86MonBrightnessUp>",
        lazy.spawn("brightnessctl set 2%+"),
        desc="Increase Brightness",
    ),
    Key(
        "<XF86MonBrightnessDown>",
        lazy.spawn("brightnessctl set 2%-"),
        desc="Reduce Brightness",
    ),
    # Key(
    #     "<XF86AudioLowerVolume>",
    #     lazy.spawn("amixer set Master 2%-"),
    #     desc="Lower the Volume",
    # ),
    # Key(
    #     "<XF86AudioRaiseVolume>",
    #     lazy.spawn("amixer set Master 2%+"),
    #     desc="Increase the Volume",
    # ),
    Key(
        "<XF86AudioLowerVolume>",
        lazy.spawn("pactl -- set-sink-volume 0 -2%"),
        desc="Lower the Volume",
    ),
    Key(
        "<XF86AudioRaiseVolume>",
        lazy.spawn("pactl -- set-sink-volume 0 +2%"),
        desc="Increase the Volume",
    ),
    Key(
        "M-<Print>",
        lazy.spawn(
            "maim -ui $(xdotool getactivewindow) ~/Desktop/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png"
        ),
        desc="Print Active Window",
    ),
    Key(
        "<Print>",
        lazy.spawn(
            "maim -u ~/Desktop/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png"
        ),
        desc="Print Entire Screen",
    ),
    Key(
        "M-S-<Print>",
        lazy.spawn(
            "maim -su ~/Desktop/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png"
        ),
        desc="Print Selected Area",
    ),
]

groups = [
    Group("1", layout="monadtall", label=WorkIcon.icon),
    Group("2", layout="monadtall", label=WorkIcon.icon),
    Group("3", layout="monadtall", label=WorkIcon.icon),
    Group("4", layout="monadtall", label=WorkIcon.icon),
    Group("5", layout="monadtall", label=WorkIcon.icon),
    Group("6", layout="monadtall", label=WorkIcon.icon),
    Group("7", layout="monadtall", label=WorkIcon.icon),
    Group("8", layout="monadtall", label=WorkIcon.icon),
    Group("9", layout="monadtall", label=WorkIcon.icon),
]

for k, group in zip(["1", "2", "3", "4", "5", "6", "7", "8", "9"], groups):
    keys.append(Key("M-" + (k), lazy.group[group.name].toscreen()))
    keys.append(Key("M-S-" + (k), lazy.window.togroup(group.name)))


def init_layout_theme():
    return {
        "margin": 4,
        "border_width": 2,
        "border_focus": "#5e81ac",
        "border_normal": "#4c566a",
    }


layout_theme = init_layout_theme()

layouts = [
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme),
    layout.Tile(ratio=0.5, **layout_theme),
    layout.MonadTall(
        ratio=0.5, new_client_position="top", single_border_width=0, **layout_theme
    ),
    layout.TreeTab(
        sections=["FIRST", "SECOND"],
        bg_color="#1b1d24",
        active_bg="#bf616a",
        inactive_bg="#a3be8c",
        padding_y=5,
        section_top=10,
        panel_width=280,
    ),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

# Colors for the bar


def init_colors():
    return [
        ["#1b1d24", "#1b1d24"],  # color 0  dark grayish blue
        ["#1b1d24", "#1b1d24"],  # color 1  dark grayish blue
        ["#3b4252", "#3b4252"],  # color 2  very dark grayish blue
        ["#434c5e", "#434c5e"],  # color 3  very dark grayish blue
        ["#4c566a", "#4c566a"],  # color 4  very dark grayish blue
        ["#d8dee9", "#d8dee9"],  # color 5  grayish blue
        ["#e5e9f0", "#e5e9f0"],  # color 6  light grayish blue
        ["#eceff4", "#eceff4"],  # color 7  light grayish blue
        ["#8fbcbb", "#8fbcbb"],  # color 8  grayish cyan
        ["#88c0d0", "#88c0d0"],  # color 9  desaturated cyan
        ["#81a1c1", "#81a1c1"],  # color 10 desaturated blue
        ["#5e81ac", "#5e81ac"],  # color 11 dark moderate blue
        ["#bf616a", "#bf616a"],  # color 12 slightly desaturated red
        ["#d08770", "#d08770"],  # color 13 desaturated red
        ["#ebcb8b", "#ebcb8b"],  # color 14 soft orange
        ["#a3be8c", "#a3be8c"],  # color 15 desaturated green
        ["#b48ead", "#b48ead"],
    ]  # color 16 grayish magenta


colors = init_colors()

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font="JetBrains Mono Nerd Font ExtraBold",
    fontsize=12,
    padding=3,
    background=colors[1],
    foreground=colors[5],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    active=colors[9],
                    inactive=colors[5],
                    borderwidth=0,
                    disable_drag=True,
                    font="JetBrains Mono Nerd Font",
                    center_aligned="True",
                    fontsize=12,
                    hide_unused=False,
                    highlight_method="text",
                    block_highlight_text_color=colors[9],
                    margin_x=2,
                    margin_y=2,
                    padding_x=5,
                    padding_y=0,
                    rounded=True,
                    this_current_screen_border=colors[14],
                    urgent_alert_method="block",
                ),
                widget.CurrentLayoutIcon(
                    background=colors[1],
                    # custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    foreground=colors[6],
                    padding=20,
                    scale=0.65,
                ),
                widget.TextBox(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    padding=0,
                    text=" ",
                ),
                widget.ThermalSensor(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    update_interval=10,
                ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=10
                ),
                # widget.CurrentLayout(
                #     background=colors[1],
                #     font='JetBrains Mono Nerd Font ExtraBold',
                #     fontsize=12,
                #     foreground=colors[6]
                # ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=10
                ),
                widget.Prompt(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                ),
                widget.Spacer(),
                widget.TextBox(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    padding=0,
                    text=" ",
                ),
                widget.Clock(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    format="%B %d, %a %I:%M %p",
                ),
                widget.Spacer(),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=10
                ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=20
                ),
                widget.TextBox(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    padding=0,
                    text=" ",
                ),
                widget.Memory(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    format="{MemPercent:.0f}%",
                    update_interval=1.0,
                ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=20
                ),
                widget.TextBox(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    padding=0,
                    text=" ",
                ),
                # widget.Volume(
                #     background=colors[1],
                #     channel="Master",
                #     device="default",
                #     fontsize=12,
                #     font="JetBrains Mono Nerd Font ExtraBold",
                # ),
                widget.PulseVolume(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    forground=colors[1],
                    channel="Master",
                    update_interval=0.006,
                    device="default",
                    step="2",
                    volume_app="pavucontrol",
                ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=20
                ),
                widget.TextBox(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    foreground=colors[6],
                    padding=0,
                    text="󰃠 ",
                ),
                widget.Backlight(
                    background=colors[1],
                    font="JetBrains Mono Nerd Font ExtraBold",
                    fontsize=12,
                    forground=colors[6],
                    backlight_name="intel_backlight",
                    brightness_file="actual_brightness",
                ),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=10
                ),
                widget.Systray(background=colors[1], icon_size=20, padding=5),
                widget.Sep(
                    background=colors[1], foreground=colors[1], linewidth=1, padding=6
                ),
                # widget.TextBox(
                #     background=colors[1],
                #     font='JetBrains Mono Nerd Font ExtraBold',
                #     fontsize=12,
                #     foreground=colors[6],
                #     padding=0,
                #     text='  '
                # ),
                # widget.Net(
                #     background=colors[1],
                #     font='JetBrains Mono Nerd Font ExtraBold',
                #     fontsize=12,
                #     foreground=colors[5],
                #     format='{interface}:{down} ↓ ',
                #     interface='all',
                #     padding=0
                # ),
            ],
            20,
            opacity=1,
            margin=[0, 0, Gaps.gap, 0],
        ),
        bottom=bar.Gap(Gaps.gap),
        right=bar.Gap(Gaps.gap),
        left=bar.Gap(Gaps.gap),
    ),
]

# Drag floating layouts.
mouse = [
    Drag("M-1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag("M-3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click("M-2", lazy.window.bring_to_front()),
]

main = None
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
    border_focus=colors[12][0],
)
auto_fullscreen = True
focus_on_window_activation = "focus"
reconfigure_screens = True


@hook.subscribe.restart
def cleanup():
    shutil.rmtree(os.path.expanduser("~/.config/qtile/__pycache__"))


@hook.subscribe.shutdown
def killall():
    shutil.rmtree(os.path.expanduser("~/.config/qtile/__pycache__"))
    subprocess.Popen(["killall", "urxvtd", "lxpolkit", "nitrogen", "picom"])


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "-cursor_name", "left_ptr"])


@hook.subscribe.client_new
def set_floating(window):
    if (
        window.window.get_wm_transient_for()
        or window.window.get_wm_type() in floating_types
    ):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
