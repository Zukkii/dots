#!/usr/bin/env bash

dir="$HOME/.config/rofi/applets/$style"
rofi_command="rofi -theme $dir/powermenu.rasi"

uptime=$(uptime -p | sed -e 's/up //g')

# Options
shutdown="󰐥"
reboot=""
lock=""
suspend="󰤄"
hibernate=""
logout="󰍃"

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$hibernate\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p " 祥 $uptime  " -dmenu -selected-row 2)"
case $chosen in
    $shutdown)
			systemctl poweroff
        ;;
    $reboot)
			systemctl reboot
        ;;
    $lock)
			i3lock-fancy -p -t \\
        ;;
    $suspend)
			mpc -q pause
			amixer set Master mute
			systemctl suspend
        ;;
    $hibernate)
      sysemctl suspend-then-hibernate
        ;;
    $logout)
				pkill -9 -f "\.\/.+\s\.|bspwm|xmonad|\[^"
        ;;
esac
