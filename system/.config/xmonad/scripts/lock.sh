#!/bin/bash

BLANK='#1B1D24'
CLEAR=$BLANK
DEFAULT='#51AFEF'
TEXT='#46D9FF'
WRONG='#bf616a'
VERIFYING='#99E269'
TYPING='#626A8C'
FONT='JetBrainsMonoExtraBold Nerd Font'

i3lock \
--time-font=$FONT           \
--verif-font=$FONT          \
--date-font=$FONT           \
--wrong-font=$FONT          \
--date-size=15              \
--verif-size=22             \
\
--insidever-color=$CLEAR     \
--ringver-color=$VERIFYING   \
--verif-align=0             \
\
--insidewrong-color=$CLEAR   \
--ringwrong-color=$WRONG     \
\
--inside-color=$BLANK        \
--ring-color=$DEFAULT        \
--line-color=$BLANK          \
--separator-color=$DEFAULT   \
\
--verif-color=$TEXT          \
--wrong-color=$TEXT          \
--time-color=$TEXT           \
--date-color=$TEXT           \
--layout-color=$TEXT         \
--keyhl-color=$TYPING        \
--bshl-color=$WRONG          \
\
--screen 1                   \
--blur 10                    \
--clock                      \
--indicator                  \
--time-str="%I:%M:%p"        \
--date-str="%A, %m %Y"       \

