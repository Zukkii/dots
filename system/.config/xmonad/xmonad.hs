-- Base
import XMonad
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

-- Actions
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves
import XMonad.Actions.WindowGo
import XMonad.Actions.WithAll
import qualified XMonad.Actions.FlexibleResize as Flex

-- Data
import Data.Char
import Data.Monoid
import Data.Maybe
import qualified Data.Tuple.Extra as TE
import qualified Data.Map as M

-- Hooks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WindowSwallowing

-- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation

-- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Simplest
import XMonad.Layout.Renamed
import XMonad.Layout.Spacing
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

-- Utilities
import XMonad.Util.EZConfig 
import XMonad.Util.Run (runProcessWithInput, safeSpawn)
import XMonad.Util.SpawnOnce
import XMonad.Util.Hacks (windowedFullscreenFixEventHook)

-- VARIABLES ------------------------------------------------------------------------

myFont :: String
myFont = "xft:JetBrainsMono ExtraBold Nerd Font Complete Mono:size=10:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask

myTerminal :: String
myTerminal = "kitty"

myEditor :: String
myEditor = myTerminal ++ "-e nvim"

myBorderWidth :: Dimension
myBorderWidth = 2

myNormColor :: String
myNormColor   = "#1B1D24"

myFocusColor :: String
myFocusColor  = "#8F9ACC"

altMask :: KeyMask
altMask = mod1Mask


-- AUTOSTART ------------------------------------------------------------------------

myStartupHook :: X ()
myStartupHook = do
  spawnOnce "ntpd &"
  spawnOnce "pipewire && pipewire-pulse && pipewire-media-session && pactl load-module module-switch-on-connect &"
  spawnOnce "xmodmap -e 'keycode 9 = Escape asciitilde Escape' &"
  spawnOnce "dunst && tuned &"
  spawnOnce "remaps && wal &" 
  spawnOnce "nm-applet && blueman-applet &"
  spawnOnce "rm ~/.xsession-errors.old ~/.xsession-errors ~/.pki ~/.ICEauthority ~/.zshrc ~/.zshrc.bak ~/.lyrics &"
  spawnOnce "DRI_PRIME=0 picom &"
  -- spawnOnce "~/.config/polybar/launch.sh &"
  spawnOnce "exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &"
  spawnOnce "feh --bg-fill --randomize --no-fehbg ~/HOME/Pictures/Wallpaper/* &"
  spawnOnce "pkill pipewire && pkill pipewire-pulse &"
  spawnOnce "pkill pipewire && pkill pipewire-pulse && pipewire-media-session && pactl load-module module-switch-on-connect &"
  spawnOnce "~/.local/bin/launch.sh"
  -- spawnOnce "exec /usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &"

-- WORKSPACES ------------------------------------------------------------------------

myWorkspaces :: [String]
myWorkspaces = clickable
               $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]


-- MANAGEHOOK ------------------------------------------------------------------------

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = (composeAll . concat $ --shifting actions
    [ [className =? "Nemo" <&&> resource =? "file_properties"    --> doCenterFloat]
    , [className =? "Nemo" <&&> resource =? "nemo-preview"    --> doCenterFloat]
    , [className =? "Thunar" <&&> appName =? "file_operation_progress"    --> doCenterFloat]
    , [className =? "Bitwarden"     --> doCenterFloat ]
    , [title     =? x --> doCenterFloatToAll | x <- important ]
    ]) <+> (composeOne . concat $     --floating actions
    [ [ className =? x -?> doCenterFloat'    | x <- cCenterF  ]
    , [ className =? x -?> doCenterFloat'    | x <- tCenterF  ]
    , [ title     =? x -?> doCenterFloat'    | x <- ptCenterF ]
    ])

  where
    important = ["New Pounces","XMPP Message Error","Polkit"]
    cCenterF  = ["confirm","file_progress","dialog","download","error","notification","pinetry-gtk-2","splash","toolbar","Xdialog","toolbar","File-roller","Nm-connection-editor","Xfce-polkit","Xfce4-","Navigator","electron","Electron","Gcolor3","GtkFileChooserDialog"]
    tCenterF  = ["Event Tester","Plugins","Add-ons","Archive manager","Chromium Options","Qalculate-gtk","Nsxiv","EMailBrowser","Sign in","About","Open files","Nemo-preview-start","GtkFileChooserDialog"]
    ptCenterF = ["About","Buddy Pounce,"]
    doMaster = doF W.shiftMaster --append this to all floats so new windows always go on top, regardless of the current focus
    doFloat' = doFloat <+> doMaster
    doCenterFloat' = doCenterFloat <+> doMaster
    doFloatAt' x y = doFloatAt x y <+> doMaster
    doSideFloat' p = doSideFloat p <+> doMaster
    doRectFloat' r = doRectFloat r <+> doMaster
    doFullFloat' = doFullFloat <+> doMaster
    doShiftAndGo ws = doF (W.greedyView ws) <+> doShift ws
    doCopyToAll = ask >>= doF . \w -> (\ws -> foldr($) ws (map (copyWindow w) myWorkspaces))
    doCenterFloatToAll = doCopyToAll <+> doCenterFloat'

-- LAYOUTS ------------------------------------------------------------------------

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
  $ smartBorders
  $ windowNavigation
  $ addTabs shrinkText myTabTheme
  $ subLayout [] (smartBorders Simplest)
  $ limitWindows 12
  $ mySpacing 3
  $ ResizableTall 1 (3/100) (1/2) []
grid     = renamed [Replace "grid"]
  $ smartBorders
  $ windowNavigation
  $ addTabs shrinkText myTabTheme
  $ subLayout [] (smartBorders Simplest)
  $ limitWindows 12
  $ mySpacing 3
  $ mkToggle (single MIRROR)
  $ Grid (16/10)
tabs     = renamed [Replace "tabs"]
  -- I cannot add spacing to this layout because it will
  -- add spacing between window and tabs which looks bad.
  $ tabbed shrinkText myTabTheme

myTabTheme = def { fontName = myFont
  , activeColor         = "#8F9ACC"
  , inactiveColor       = "#1B1D24"
  , activeBorderColor   = "#8F9ACC"
  , inactiveBorderColor = "#1B1D24"
  , activeTextColor     = "#1B1D24"
  , inactiveTextColor   = "#d0d0d0"
  }

-- The layout hook

myLayoutHook = avoidStruts $ 
  mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
  myDefaultLayout = withBorder myBorderWidth tall
                ||| noBorders tabs
                ||| withBorder myBorderWidth grid


-- KEYBINDINGS ------------------------------------------------------------------------

myKeys :: [(String, X ())]
myKeys =

-- START_KEYS

  -- Xmonad
  [ ("M-C-r", spawn "xmonad --recompile")     -- Recompile Xmonad
  , ("M-S-r", spawn "xmonad --restart")       -- Restart Xmonad
  , ("C-q", io exitSuccess)

  -- Lock
  , ("M-M1-l", spawn "bash ~/.xmonad/scripts/lock.sh")    -- Lock Desktop

  -- Open my preferred terminal
  , ("M-<Return>", spawn myTerminal)      -- Open Terminal

  -- KB_GROUP Run Scripts
  , ("M-S-<Return>", spawn "bash ~/.config/rofi/scripts/appsmenu.sh")  -- Rofi App Launcher
  , ("M-S-q", spawn "bash ~/.config/rofi/scripts/powermenu.sh")       -- Rofi Power Menu
  , ("M-w", spawn "feh --bg-fill --randomize --no-fehbg ~/HOME/Pictures/Wallpaper/*")           -- Change Wallpaper

  -- Windows
  , ("M-S-c", kill1)                           -- Kill the currently focused client
  , ("M-S-a", killAll)                         -- Kill all windows on current workspace

  -- Floating windows
  , ("M-<Backspace>", withFocused $ windows . W.sink) -- Push floating window back to tile
  , ("M-S-<Backspace>", sinkAll)                      -- Push ALL floating windows to tile

  -- Windows navigation
  , ("M-m", windows W.focusMaster)     -- Move focus to the master window
  , ("M-j", windows W.focusDown)       -- Move focus to the next window
  , ("M-k", windows W.focusUp)         -- Move focus to the prev window
  --, ("M-S-m", windows W.swapMaster)    -- Swap the focused window and the master window
  , ("M-S-j", windows W.swapDown)      -- Swap focused window with next window
  , ("M-S-k", windows W.swapUp)        -- Swap focused window with prev window
  -- , ("M-<Backspace>", promote)         -- Moves focused window to master, others maintain order
  , ("M1-S-<Tab>", rotSlavesDown)      -- Rotate all windows except master and keep focus in place
  , ("M1-C-<Tab>", rotAllDown)         -- Rotate all the windows in the current stack
  , ("M-C-s", killAllOtherCopies)

  -- Layouts
  , ("M-<Tab>", sendMessage NextLayout)                -- Switch to next layout
  , ("M-C-M1-<Up>", sendMessage Arrange)
  , ("M-C-M1-<Down>", sendMessage DeArrange)
  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  , ("M-S-<Space>", sendMessage ToggleStruts)         -- Toggles struts
  , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)      -- Toggles noborder
  , ("M-<KP_Multiply>", sendMessage (IncMasterN 1))   -- Increase number of clients in master pane
  , ("M-<KP_Divide>", sendMessage (IncMasterN (-1)))  -- Decrease number of clients in master pane
  , ("M-S-<KP_Multiply>", increaseLimit)              -- Increase number of windows
  , ("M-S-<KP_Divide>", decreaseLimit)                -- Decrease number of windows
  , ("M-h", sendMessage Shrink)                       -- Shrink horiz window width
  , ("M-l", sendMessage Expand)                       -- Expand horiz window width
  , ("M-M1-j", sendMessage MirrorShrink)               -- Shrink vert window width
  , ("M-M1-k", sendMessage MirrorExpand)               -- Exoand vert window width

  -- This is used to push windows to tabbed sublayouts, or pull them out of it.
  , ("M-C-h", sendMessage $ pullGroup L)
  , ("M-C-l", sendMessage $ pullGroup R)
  , ("M-C-k", sendMessage $ pullGroup U)
  , ("M-C-j", sendMessage $ pullGroup D)
  , ("M-C-m", withFocused (sendMessage . MergeAll))
  -- , ("M-C-u", withFocused (sendMessage . UnMerge))
  , ("M-C-/", withFocused (sendMessage . UnMergeAll))
  , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
  , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab

  -- Workspaces
  , ("M-.", nextScreen)  -- Switch focus to next monitor
  , ("M-,", prevScreen)  -- Switch focus to prev monitor
  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

  -- Brightness Keys
  , ("<XF86MonBrightnessUp>", spawn "brightnessctl set 2%+")      -- Brightness Up
  , ("<XF86MonBrightnessDown>", spawn "brightnessctl set 2%-")    -- Brightness Down
  , ("<XF86AudioPrev>", spawn "brightnessctl set 2%+")      -- Brightness Up

  -- Screenshot
  -- Screenshot active Window Only
  , ("M-<Print>", spawn "maim -ui $(xdotool getactivewindow) ~/HOME/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png")
  -- Screenshot Everything
  , ("<Print>", spawn "maim -u ~/HOME/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png")
  -- Screenshot Selected Area
  , ("M-S-<Print>", spawn "maim -su ~/HOME/Pictures/Screenshot/screenshot$(date +%Y-%m-%d-%H:%M:%S).png")

  -- Restart Polybar
  , ("M-S-p", spawn "~/.local/bin/launch.sh")

  -- Multimedia Keys
  , ("<XF86AudioPlay>", spawn "cmus toggle")      -- Play
  , ("<XF86AudioPrev>", spawn "cmus prev")        -- Play Previous
  , ("<XF86AudioNext>", spawn "cmus next")        -- Play Next
  , ("<XF86AudioMute>",   spawn "pactl set-sink-mute 0 toggle")       -- Audio Mute
  , ("<XF86AudioLowerVolume>", spawn "pactl -- set-sink-volume 0 -2%")        -- Lower Volume
  , ("<XF86AudioRaiseVolume>", spawn "pactl -- set-sink-volume 0 +2%")        -- Raise Volume
  , ("<XF86Calculator>", runOrRaise "gcalctool" (resource =? "gcalctool"))

  ]

------------------------------------------------------------------------
-- Moving between WS
------------------------------------------------------------------------

          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))


myMouse (XConfig {XMonad.modMask = modMask}) = M.fromList $
  [ ((modMask, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)) -- set the window to floating mode and move by dragging
  , ((mod1Mask, button1), (\w -> focus w >> Flex.mouseResizeWindow w)) -- set the window to floating mode and resize by dragging
  , ((modMask, button3), (\w -> focus w >> windows W.shiftMaster)) -- raise the window to the top of the stack
  ]

-- END_KEYS

-- MAIN ------------------------------------------------------------------------

main :: IO ()
main = do
  xmonad $ ewmh $ docks $ defaults
defaults = def
  { modMask             = myModMask
  , terminal            = myTerminal
  , handleEventHook     = windowedFullscreenFixEventHook <> swallowEventHook (className =? "kitty"  <||> className =? "st-256color" <||> className =? "XTerm" <||> className =? "mpv" <||> className =? "nemo") (return True)
  , workspaces          = myWorkspaces
  , layoutHook          = smartBorders $ myLayoutHook
  , normalBorderColor   = myNormColor
  , focusedBorderColor  = myFocusColor
  , manageHook          = myManageHook <+> manageHook def
  , borderWidth         = myBorderWidth
  , startupHook         = myStartupHook
  , mouseBindings       = myMouse
  } `additionalKeysP` myKeys
