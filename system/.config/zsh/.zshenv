# Profile File. Runs on login.

# Environmental variables sets in here
typeset -U PATH

# Adds ~/.local/bin to PATH
export PATH="$HOME/.local/bin:$PATH"

# Default Programs
export EDITOR="nvim"
export VISUAL="nvim"
export TERMINAL="kitty"
export BROWSER="microsoft-edge-stable"
export READER="zathura"
export IMAGE="xviewer"
export FILEMANAGER="nemo"
export WM="xmonad"

#XDG BASE Directory
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_LOCAL_HOME="$HOME/.local"

# Disable Files
export LESSHISTFILE="-"

# # QT5
# export QT_QPA_PLATFORMTHEME=qt5ct

export QT_QPA_PLATFORMTHEME=qt5ct
# export QT_STYLE_OVERRIDE=qt5ct

# Set Qt to use GTK theme
#export QT_QPA_PLATFORMTHEME="gtk2"

# Npm path
export PATH="$HOME/.config/node_modules/bin:$PATH"
# export NPM_CONFIG_PREFIX="$HOME/.config/node_modules"
export NPM_CONFIG_USERCONFIG="$HOME/.config/npm/npmrc"
export NPM_CONFIG_CACHE="$HOME/.cache/npm"
export NPM_CONFIG_TMP="$XDG_RUNTIME_DIR/npm"

# Cargo
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# Gradle
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"

#gtk2
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc


# ~/ Clean-up Home Directory:
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export XMONAD_CONFIG_HOME="$HOME/.config/xmonad"
export XMONAD_DATA_HOME="$XDG_DATA_HOME/xmonad"
export XMONAD_CACHE_HOME="$XDG_CACHE_HOME/xmonad"
# export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GEM_HOME="$XDG_DATA_HOME/gem"
export GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
export STACK_ROOT="$XDG_DATA_HOME/stack"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export MOCDIR="$XDG_CONFIG_HOME/moc"
export MOCDir="$XDG_CONFIG_HOME/moc"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"
#export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
#export ANDROID_AVD_HOME="${XDG_DATA_HOME:-$HOME/.config}/android/"
#export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME/android/"
export ADB_VENDOR_KEYS="$XDG_CONFIG_HOME/android"
export STARSHIP_CACHE="$XDG_CACHE_HOME/starship"
export ERRFILE="$XDG_CACHE_HOME/x11/"
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export VSCODE_PORTABLE="$XDG_DATA_HOME/vscode"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export gnome_user_dir="$XDG_DATA_HOME/gnome/apps"
export ALSA="XDG_CONFIG_HOME/alsa/asoundrc"

export LEIN_HOME="$XDG_DATA_HOME/m2"
export PYENV_ROOT="$XDG_DATA_HOME/pyenv"
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"

#export FZF_DEFAULT_OPTS="--layout=reverse --height 60%"

xrdb -load ~/.config/X11/.Xresources

export XCURSOR_PATH=${XCURSOR_PATH}:~/.local/share/icons
#export XCURSOR_PATH="$XCURSOR_PATH/home/masaomi/.local/share/icons"

# FZF Setup
export FZF_DEFAULT_OPTS='
--color fg:7,hl:4,fg+:15,bg+:0,hl+:3
--color pointer:1,info:8,spinner:3,header:8,prompt:4,marker:8
--info=inline
--height 40%
--cycle
--reverse
--pointer=">"
'
export FZF_DEFAULT_COMMAND="find . -mindepth 1 2>/dev/null"
export FZF_ALT_C_COMMAND="find . -mindepth 1 -type d 2>/dev/null"
export FZF_ALT_C_OPTS="$FZF_DEFAULT_OPTS"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="$FZF_DEFAULT_OPTS"

# ANDROID

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# NVM
# export NVM_HOME="$XDG_CONFIG_HOME/nvm" 2>/dev/null
# export PATH=${PATH}:${NVM_HOME} 2>/dev/null
# source ${NVM_HOME}/nvm.sh 2>/dev/null


export REPLICATE_API_TOKEN=r8_SzFTa52ySNWUXZKvabvqfLJJlGrr1Dn1MpFj4
