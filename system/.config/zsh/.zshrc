# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

export HISTSIZE=1000000
export HISTFILESIZE=1000000
export SAVEHIST=1000000
export HISTFILE=~/.local/share/zsh/history/.zsh_history
setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt INC_APPEND_HISTORY
export HISTTIMEFORMAT="[%F %T] "
export TERM="xterm-256color"
setopt autocd    # Automatically cd into typed directory.
setopt completealiases
setopt auto_menu         # Automatically use menu completion
setopt always_to_end     # Move cursor to end if word had one match
setopt complete_in_word  # Completion from both ends
setopt extendedglob  # Additional syntax for filename generation

ZSH_THEME=eastwood

# autoload -U colors && colors	# Load colors
# setopt PROMPT_SUBST
# # PROMPT='%{$fg[yellow]%}[%{$fg[cyan]%}%n%{$fg[yellow]%}@%{$fg[magenta]%}%M%{$fg[yellow]%}]%{$fg[cyan]%}(%{$fg[cyan]%}%~%{$fg[cyan]%})$(gitstatus)%{$fg[yellow]%}  '
# PROMPT='%{$fg[cyan]%}[%{$fg[magenta]%}%1d%{$fg[cyan]%}]%F{red}${vcs_info_msg_0_}%f%{$fg[yellow]%}>> '
#
# Basic auto/tab complete:
autoload -Uz compinit && compinit -u
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' ignored-patterns '.'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'
_comp_options+=(globdots) # Include hidden files

# Git status ZSH PROMPT
autoload -Uz add-zsh-hook
autoload -Uz add-zsh-hook vcs_info
add-zsh-hook precmd vcs_info
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
zstyle ':vcs_info:git:*' formats       '(%b%u%c)'
zstyle ':vcs_info:git:*' actionformats '(%b|%a%u%c)'

# Path to your oh-my-zsh installation.
export ZSH="$ZDOTDIR/ohmyzsh"

plugins=(
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
  fast-syntax-highlighting
  # zsh-autocomplete
  zsh-recall
)

source $ZSH/oh-my-zsh.sh

# Load aliases
source ~/.config/zsh/aliasrc 2>/dev/null

lfcd() {
    tmp="$(mktemp)"
    ~/.config/lf/lf-ueberzug -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp" >/dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir" && ls -hNA --color=auto --group-directories-first
    fi
}

bindkey -s '^v' 'lfcd\n'
bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'

remaps
